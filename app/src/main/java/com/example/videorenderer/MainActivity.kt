package com.example.videorenderer

import android.media.MediaPlayer
import android.opengl.EGL14
import android.opengl.GLSurfaceView
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.videorenderer.effects.BlurEffect
import com.example.videorenderer.effects.NoEffect
import kotlinx.android.synthetic.main.activity_main.*
import javax.microedition.khronos.egl.EGL10
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.egl.EGLContext
import javax.microedition.khronos.egl.EGLDisplay

class MainActivity : AppCompatActivity() {
    private val mediaPlayer = MediaPlayer()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setupMediaPlayer()
        setupSurface()
    }

    private fun setupMediaPlayer() {
        mediaPlayer.isLooping = true
        val assetFileDescriptor = assets.openFd("video.mp4")

        mediaPlayer.setDataSource(
            assetFileDescriptor.fileDescriptor,
            assetFileDescriptor.startOffset,
            assetFileDescriptor.length
        )

        mediaPlayer.setOnErrorListener { _, what, extra ->
            false
        }
    }

    private fun setupSurface() {
        // https://github.com/DeepARSDK/videocall-android-java/blob/master/app/src/main/java/ai/deepar/videocall/DeepARRenderer.java
        val contextFactory = object : GLSurfaceView.EGLContextFactory {

            override fun createContext(
                egl: EGL10,
                display: EGLDisplay,
                config: EGLConfig
            ): EGLContext {
                val attributes = intArrayOf(
                    EGL14.EGL_CONTEXT_CLIENT_VERSION,
                    2,
                    EGL10.EGL_NONE
                )
                return egl.eglCreateContext(display, config, EGL10.EGL_NO_CONTEXT, attributes)
            }

            override fun destroyContext(egl: EGL10, display: EGLDisplay, context: EGLContext) {
                if (!egl.eglDestroyContext(display, context)) {
                    //
                }
            }
        }

        surfaceVideo.init(mediaPlayer, NoEffect(), contextFactory)

        swBlur.setOnCheckedChangeListener { _, isChecked ->
            surfaceVideo.shader = if (isChecked) {
                BlurEffect(7, surfaceVideo.width / 4, surfaceVideo.height / 4)
            } else {
                null
            }
        }
    }
}